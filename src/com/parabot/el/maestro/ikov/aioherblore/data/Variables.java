package com.parabot.el.maestro.ikov.aioherblore.data;

import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Task;
import org.parabot.core.ui.Logger;

import java.util.ArrayList;

public class Variables {

    private static Task currentTask;
    private static int flasksMade;
    private static int herbsCleaned;
    private static int ingredientsGrinded;
    private static int potionBankCount;
    private static int potionsMade;
    private static String status = "Loading up...";
    private static ArrayList<Task> tasks;
    private static int unfMade;

    public static void addTask(final Task task) {
        if (tasks == null) {
            tasks = new ArrayList<Task>();
            tasks.add(new Task(Action.TASK_TRANSITION));
        }
        tasks.add(task);
        tasks.add(new Task(Action.TASK_TRANSITION));
    }

    public static int getActionsPerformed() {
        return flasksMade + potionsMade + herbsCleaned + unfMade + ingredientsGrinded;
    }

    public static Task getCurrentTask() {
        return currentTask;
    }

    public static void setCurrentTask(final Task currentTask) {
        Variables.currentTask = currentTask;
    }

    public static int getFlasksMade() {
        return flasksMade;
    }

    public static void setFlasksMade(final int flasksMade) {
        Variables.flasksMade = flasksMade;
    }

    public static int getHerbsCleaned() {
        return herbsCleaned;
    }

    public static void setHerbsCleaned(final int herbsCleaned) {
        Variables.herbsCleaned = herbsCleaned;
    }

    public static int getIngredientsGrinded() {
        return ingredientsGrinded;
    }

    public static void setIngredientsGrinded(final int ingredientsGrinded) {
        Variables.ingredientsGrinded = ingredientsGrinded;
    }

    public static int getPotionBankCount() {
        return potionBankCount;
    }

    public static void setPotionBankCount(final int pootionBankCount) {
        Variables.potionBankCount = pootionBankCount;
    }

    public static int getPotionsMade() {
        return potionsMade;
    }

    public static void setPotionsMade(final int potionsMade) {
        Variables.potionsMade = potionsMade;
    }

    public static String getStatus() {
        return status;
    }

    public static ArrayList<Task> getTasks() {
        return tasks;
    }

    public static int getUnfMade() {
        return unfMade;
    }

    public static void setUnfMade(final int i) {
        unfMade = i;
    }

    public static void setStatus(final String s, final boolean log) {
        if (!status.equalsIgnoreCase(s)) {
            status = s;
            if (log) {
                Logger.addMessage(s);
            }
        }
    }

}
