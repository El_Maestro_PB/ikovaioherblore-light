package com.parabot.el.maestro.ikov.aioherblore.data;

import com.parabot.el.maestro.ikov.aioherblore.util.Ingredient;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public enum Herb implements Ingredient {

    CLEAN_AVANTOE("Clean Avantoe", 262), CLEAN_CADANTINE("Clean Cadantine", 266), CLEAN_DWARF_WEED("Clean Dwarf Weed", 268), CLEAN_FELLSTALK("Clean Fellstalk", 21625), CLEAN_GUAM(
            "Clean Guam", 250), CLEAN_HARRALANDER("Clean Harralander", 256), CLEAN_IRIT("Clean Irit", 260), CLEAN_KWUARM("Clean Kwuarm", 264), CLEAN_LANTADYME("Clean Lantadyme",
            2482), CLEAN_MARRENTILL("Clean Marrentill", 252), CLEAN_RANARR("Clean Ranarr", 258), CLEAN_SNAPDRAGON("Clean Snapdragon", 3001), CLEAN_TARROMIN("Clean Tarromin", 254), CLEAN_TOADFLAX(
            "Clean Toadflax", 2999), CLEAN_TORSTOL("Clean Torstol", 270), GRIMY_AVANTOE("Grimy Avantoe", 212), GRIMY_CADANTINE("Grimy Cadantine", 216), GRIMY_DWARF_WEED(
            "Grimy Dwarf Weed", 218), GRIMY_FELLSTALK("Grimy Fellstalk", 21627), GRIMY_GUAM("Grimy Guam", 200), GRIMY_HARRALANDER("Grimy Harralander", 206), GRIMY_IRIT(
            "Grimy Irit", 210), GRIMY_KWUARM("Grimy Kwuarm", 214), GRIMY_LANTADYME("Grimy Lantadyme", 2486), GRIMY_MARRENTILL("Grimy Marrentill", 202), GRIMY_RANARR(
            "Grimy Ranarr", 208), GRIMY_SNAPDRAGON("Grimy Snapdragon", 3052), GRIMY_TARROMIN("Grimy Tarromin", 204), GRIMY_TOADFLAX("Grimy Toadflax", 3050), GRIMY_TORSTOL(
            "Grimy Torstol", 220);

    private int id;
    private String name;

    Herb(final String name, final int id) {
        this.name = name;
        this.id = id;
    }

    public int getBankCount() {
        if (!Bank.isOpen()) {
            return 0;
        }
        return Bank.getCount(id);
    }


    public int getBankStackSize() {
        if (!Bank.isOpen()) {
            return 0;
        }
        final Item item = Bank.getItem(id);
        if (item != null) {
            return Bank.getItem(id).getStackSize();
        }
        return 0;
    }

    public Herb getCounterPart() {
        for (final Herb herb : values()) {
            if (herb.name.contains(name.split(" ")[1]) && herb.getId() != id) {
                return herb;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public int getInventoryCount() {
        return Inventory.getCount(id);
    }

    public int getInventoryStackSize() {
        final Item item = Inventory.getItem(id);
        if (item != null) {
            return Inventory.getItem(id).getStackSize();
        }
        return 0;
    }

    public String getName() {
        return name;
    }

    public boolean hasAmountInBag(final int amount) {
        return Inventory.getCount(id) >= amount;
    }

    public boolean hasAmountInBank(final int amount) {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(id) >= amount;
    }

    public boolean hasInBag() {
        return Inventory.getCount(id) > 0;
    }

    public boolean hasInBank() {
        return Bank.isOpen() && Bank.getCount(id) > 0;
    }
}
