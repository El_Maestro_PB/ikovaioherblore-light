package com.parabot.el.maestro.ikov.aioherblore.data;

import com.parabot.el.maestro.ikov.aioherblore.util.Ingredient;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public enum UnfinishedPotion implements Ingredient {

    AVANTOE_POTION_UNF("Avantoe Potion (unf)", 104, Herb.CLEAN_AVANTOE), CADANTINE_POTION_UNF("Cadantine Potion (unf)", 108, Herb.CLEAN_CADANTINE), DWARF_WEED_POTION_UNF(
            "Dwarf Weed Potion (unf)", 110, Herb.CLEAN_DWARF_WEED), FELLSTALK_POTION_UNF("Fellstalk Potion (unf)", 21629, Herb.CLEAN_FELLSTALK), GUAM_POTION_UNF(
            "Guam Potion (unf)", 92, Herb.CLEAN_GUAM), HARRALANDER_POTION_UNF("Harralander Potion (unf)", 98, Herb.CLEAN_HARRALANDER), IRIT_POTION_UNF("Irit Potion (unf)", 102,
            Herb.CLEAN_IRIT), KWUARM_POTION_UNF("Kwuarm Potion (unf)", 106, Herb.CLEAN_KWUARM), LANTADYME_POTION_UNF("Lantadyme Potion (unf)", 2484, Herb.CLEAN_LANTADYME), MARRENTILL_POTION_UNF(
            "Marrentill Potion (unf)", 94, Herb.CLEAN_MARRENTILL), RANARR_POTION_UNF("Ranarr Potion (unf)", 100, Herb.CLEAN_RANARR), SNAPDRAGON_POTION_UNF(
            "Snapdragon Potion (unf)", 3005, Herb.CLEAN_SNAPDRAGON), TARROMIN_POTION_UNF("Tarromin Potion (unf)", 96, Herb.CLEAN_TARROMIN), TOADFLAX_POTION_UNF(
            "Toadflax Potion (unf)", 3003, Herb.CLEAN_TOADFLAX), TORSTOL_POTION_UNF("Torstol Potion (unf)", 112, Herb.CLEAN_TORSTOL);

    private Herb herbNeeded;
    private int id;
    private String name;

    UnfinishedPotion(final String name, final int id, final Herb herbNeeded) {
        this.name = name;
        this.id = id;
        this.herbNeeded = herbNeeded;
    }

    public boolean canMake() {
        return herbNeeded.hasInBag() && Secondary.VIAL.hasInBag();
    }

    @Override
    public int getBankCount() {
        if (!Bank.isOpen()) {
            return 0;
        }
        return Bank.getCount(id);
    }

    @Override
    public int getBankStackSize() {
        if (!Bank.isOpen()) {
            return 0;
        }
        final Item item = Bank.getItem(id);
        if (item != null) {
            return Bank.getItem(id).getStackSize();
        }
        return 0;
    }

    public Herb getHerbNeeded() {
        return herbNeeded;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getInventoryCount() {
        return Inventory.getCount(id);
    }

    @Override
    public int getInventoryStackSize() {
        final Item item = Inventory.getItem(id);
        if (item != null) {
            return Inventory.getItem(id).getStackSize();
        }
        return 0;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean hasAmountInBag(final int amount) {
        return Inventory.getCount(id) >= amount;
    }

    @Override
    public boolean hasAmountInBank(final int amount) {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(id) >= amount;
    }

    @Override
    public boolean hasInBag() {
        return Inventory.getCount(id) > 0;
    }

    @Override
    public boolean hasInBank() {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(id) > 0;
    }

}
