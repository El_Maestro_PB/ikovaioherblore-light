package com.parabot.el.maestro.ikov.aioherblore.data;

public class Constants {
    public static final int MAKE_DIALOG = 4429;
    public static final int PESTLE_AND_MORTAR = 234;
    public static final String[] PREMIUM_DECANT_GUI_OPTIONS = {"Super Attack Potion", "Super Strength Potion", "Super Defence Potion", "Magic Potion", "Range Potion",
            "Super Restore Potion", "Recover Special Potion", "Super Prayer Potion", "Saradomin Brew", "Extreme Strength Potion", "Extreme Attack Potion", "Extreme Range Potion",
            "Extreme Defence Potion", "Extreme Magic Potion", "Prayer Renewal Potion", "Overload"};
    public static final String[] PREMIUM_MAKE_GUI_OPTIONS = {"Recover Special Potion", "Super Prayer Potion", "Extreme Strength Potion", "Extreme Attack Potion",
            "Extreme Range Potion", "Extreme Defence Potion", "Extreme Magic Potion", "Overload"};
    public static final double VERSION = 1.9;
    public static final String VERSION_TYPE = "Light";

}