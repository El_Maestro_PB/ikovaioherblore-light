package com.parabot.el.maestro.ikov.aioherblore.data;

import com.parabot.el.maestro.ikov.aioherblore.util.Ingredient;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public enum Secondary implements Ingredient {

    BLUE_DRAGON_SCALE("Blue Dragon Scale", 244), CHOCOLATE_DUST("Chocolate Dust", 1976), CRUSHED_NEST("Crushed Nest", 6694), DRAGON_SCALE_DUST("Dragon Scale Dust", 242), EYE_OF_NEWT(
            "Eye of Newt", 222), GOAT_HORN_DUST("Goat Horn Dust", 9737), GRENWALL_SPIKES("Grenwall Spikes", 12540), GROUND_MUD_RUNE("Ground Mud Rune", 9595), LIMPWUR_ROOT(
            "Limpwurt Root", 226), MORT_MYRE_FUNGUS("Mort Myre Fungus", 2971), MUD_RUNE("Mud Rune", 4699), PAPAYA_FRUIT("Papaya Fruit", 5973), PHOENIX_FEATHER("Phoenix Feather",
            4622), POTATO_CACTUS("Potato Cactus", 3139), RED_SPIDERS_EGGS("Red Spider's Eggs", 224), SNAPE_GRASS("Snape Grass", 232), UNICORN_HORN("Unicorn Horn", 238), UNICORN_HORN_DUST(
            "Unicorn Horn Dust", 236), VIAL("Vial", 228), WHITE_BERRIES("White Berries", 240), WINE_OF_ZAMORAK("Wine of Zamorak", 246), WYVERN_BONES("Wyvern Bones", 6813);

    private int id;
    private String name;

    Secondary(final String name, final int id) {
        this.id = id;
        this.name = name;
    }

    public boolean canGrind() {
        return hasInBag() && Inventory.getCount(Constants.PESTLE_AND_MORTAR) >= 1;
    }

    @Override
    public int getBankCount() {
        if (!Bank.isOpen()) {
            return 0;
        }
        return Bank.getCount(id);
    }

    @Override
    public int getBankStackSize() {
        if (!Bank.isOpen()) {
            return 0;
        }
        final Item item = Bank.getItem(id);
        if (item != null) {
            return Bank.getItem(id).getStackSize();
        }
        return 0;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getInventoryCount() {
        return Inventory.getCount(id);
    }

    @Override
    public int getInventoryStackSize() {
        final Item item = Inventory.getItem(id);
        if (item != null) {
            return Inventory.getItem(id).getStackSize();
        }
        return 0;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean hasAmountInBag(final int amount) {
        return Inventory.getCount(id) >= amount;
    }

    @Override
    public boolean hasAmountInBank(final int amount) {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(id) >= amount;
    }

    @Override
    public boolean hasInBag() {
        return Inventory.getCount(id) > 0;
    }

    @Override
    public boolean hasInBank() {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(id) > 0;
    }

}
