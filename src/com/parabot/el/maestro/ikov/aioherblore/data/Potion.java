package com.parabot.el.maestro.ikov.aioherblore.data;

import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Ingredient;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public enum Potion implements Ingredient {

    ATTACK_POTION("Attack Potion", UnfinishedPotion.GUAM_POTION_UNF, 122, 2429, Secondary.EYE_OF_NEWT, Herb.CLEAN_GUAM), ANTI_POISON("Anti-Poison",
            UnfinishedPotion.MARRENTILL_POTION_UNF, 176, 2447, Secondary.UNICORN_HORN_DUST, Herb.CLEAN_MARRENTILL), RESTORE_POTION("Restore Potion",
            UnfinishedPotion.HARRALANDER_POTION_UNF, 128, 2431, Secondary.RED_SPIDERS_EGGS, Herb.CLEAN_HARRALANDER), PRAYER_POTION("Prayer Potion",
            UnfinishedPotion.RANARR_POTION_UNF, 140, 2435, Secondary.SNAPE_GRASS, Herb.CLEAN_RANARR), SUPER_ENERGY_POTION("Super Energy Potion",
            UnfinishedPotion.AVANTOE_POTION_UNF, 3019, 3017, Secondary.MORT_MYRE_FUNGUS, Herb.CLEAN_AVANTOE), SUPER_STRENGTH_POTION("Super Strength Potion",
            UnfinishedPotion.KWUARM_POTION_UNF, 158, 2441, Secondary.LIMPWUR_ROOT, Herb.CLEAN_KWUARM), SUPER_ATTACK_POTION("Super Attack Potion", UnfinishedPotion.IRIT_POTION_UNF,
            146, 2437, Secondary.EYE_OF_NEWT, Herb.CLEAN_IRIT), SUPER_RESTORE_POTION("Super Restore Potion", UnfinishedPotion.SNAPDRAGON_POTION_UNF, 3027, 3025,
            Secondary.RED_SPIDERS_EGGS, Herb.CLEAN_SNAPDRAGON), SUPER_DEFENCE_POTION("Super Defence Potion", UnfinishedPotion.CADANTINE_POTION_UNF, 164, 2443,
            Secondary.WHITE_BERRIES, Herb.CLEAN_CADANTINE), RANGE_POTION("Range Potion", UnfinishedPotion.DWARF_WEED_POTION_UNF, 170, 2445, Secondary.WINE_OF_ZAMORAK,
            Herb.CLEAN_DWARF_WEED), MAGIC_POTION("Magic Potion", UnfinishedPotion.LANTADYME_POTION_UNF, 3043, 3041, Secondary.POTATO_CACTUS, Herb.CLEAN_LANTADYME), SARADOMIN_BREW(
            "Saradomin Brew", UnfinishedPotion.TOADFLAX_POTION_UNF, 6688, 6686, Secondary.CRUSHED_NEST, Herb.CLEAN_TOADFLAX), ANTIFIRE_POTION("Antifire Potion", 2455, 2453,
            Secondary.CHOCOLATE_DUST, Herb.CLEAN_AVANTOE), RECOVER_SPECIAL_POTION("Recover Special Potion", 15302, 15300, SUPER_ENERGY_POTION, Secondary.PAPAYA_FRUIT), EXTREME_STRENGTH_POTION(
            "Extreme Strength Potion", 15314, 15213, SUPER_STRENGTH_POTION, Herb.CLEAN_DWARF_WEED), EXTREME_RANGE_POTION("Extreme Range Potion", 15326, 15324, RANGE_POTION,
            Secondary.GRENWALL_SPIKES), EXTREME_MAGIC_POTION("Extreme Magic Potion", 15322, 15320, MAGIC_POTION, Secondary.GROUND_MUD_RUNE), EXTREME_ATTACK_POTION(
            "Extreme Attack Potion", 15310, 1, SUPER_ATTACK_POTION, Herb.CLEAN_AVANTOE), EXTREME_DEFENCE_POTION("Extreme Defence Potion", 15318, 15316, SUPER_DEFENCE_POTION,
            Herb.CLEAN_LANTADYME), SUPER_PRAYER_POTION("Super Prayer Potion", 15330, 15328, PRAYER_POTION, Secondary.WYVERN_BONES), SUPER_ANTIFIRE_POTION("Super Antifire Potion",
            15306, 15304, ANTIFIRE_POTION, Secondary.PHOENIX_FEATHER), PRAYER_RENEWAL_POTION("Prayer Renewal Potion", UnfinishedPotion.FELLSTALK_POTION_UNF, 21633, 21631, Secondary.MORT_MYRE_FUNGUS, Herb.CLEAN_FELLSTALK), OVERLOAD("Overload", 15334, 15332, Herb.CLEAN_TORSTOL, EXTREME_MAGIC_POTION, EXTREME_RANGE_POTION,
            EXTREME_ATTACK_POTION, EXTREME_STRENGTH_POTION, EXTREME_DEFENCE_POTION);

    private int $3DoseId;
    private int $4DoseId;
    private boolean extreme;
    private Ingredient[] ingredients;
    private String name;
    private boolean overload;
    private Ingredient primary;
    private Ingredient secondary;
    private UnfinishedPotion unfinishedPotion;

    Potion(final String name, final int $3DoseId, final int $4DoseId, final Ingredient... ingredients) {
        this.ingredients = ingredients;
        this.name = name;
        this.$3DoseId = $3DoseId;
        this.$4DoseId = $4DoseId;
        overload = true;
    }

    Potion(final String name, final int $3DoseId, final int $4DoseId, final Ingredient primary, final Ingredient secondary) {
        this.name = name;
        this.$3DoseId = $3DoseId;
        this.$4DoseId = $4DoseId;
        this.secondary = secondary;
        this.primary = primary;
        extreme = true;
    }

    Potion(final String name, final UnfinishedPotion unfinishedPotion, final int $3DoseId, final int $4DoseId, final Ingredient secondary, final Ingredient primary) {
        this.unfinishedPotion = unfinishedPotion;
        this.name = name;
        this.$3DoseId = $3DoseId;
        this.$4DoseId = $4DoseId;
        this.secondary = secondary;
        this.primary = primary;
        extreme = false;
        overload = false;
    }

    public boolean canDecant() {
        if (getId() == $4DoseId) {
            return Inventory.getCount($4DoseId) >= 3;
        }
        return Inventory.getCount($3DoseId) >= 2;
    }

    public boolean canMake() {
        if (isExtreme()) {
            return secondary.hasInBag() && primary.hasInBag();
        } else if (isOverload()) {
            for (final Ingredient in : ingredients) {
                if (!in.hasInBag()) {
                    return false;
                }
            }
            return true;
        }
        return secondary.hasInBag() && unfinishedPotion.hasInBag();
    }

    @Override
    public int getBankCount() {
        if (!Bank.isOpen()) {
            return 0;
        }
        return Bank.getCount(getId());
    }

    @Override
    public int getBankStackSize() {
        if (!Bank.isOpen()) {
            return 0;
        }
        final Item item = Bank.getItem(getId());
        if (item != null) {
            return Bank.getItem(getId()).getStackSize();
        }
        return 0;
    }

    @Override
    public int getId() {
        if (Variables.getCurrentTask().getAction() == Action.DECANT_POTION) {
            if (Variables.getCurrentTask().getDoseToDecant() == 3) {
                return $3DoseId;
            }
            return $4DoseId;
        }
        return $3DoseId;
    }

    public Ingredient[] getIngredients() {
        return ingredients;
    }

    @Override
    public int getInventoryCount() {
        return Inventory.getCount(getId());
    }

    @Override
    public int getInventoryStackSize() {
        final Item item = Inventory.getItem(getId());
        if (item != null) {
            return Inventory.getItem(getId()).getStackSize();
        }
        return 0;
    }

    @Override
    public String getName() {
        return name;
    }

    public Ingredient getPrimary() {
        return primary;
    }

    public Ingredient getSecondary() {
        return secondary;
    }

    public UnfinishedPotion getUnfinishedPotion() {
        return unfinishedPotion;
    }

    @Override
    public boolean hasAmountInBag(final int amount) {
        return Inventory.getCount(getId()) >= amount;
    }

    @Override
    public boolean hasAmountInBank(final int amount) {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(getId()) >= amount;
    }

    @Override
    public boolean hasInBag() {
        return Inventory.getCount(getId()) > 0;
    }

    @Override
    public boolean hasInBank() {
        if (!Bank.isOpen()) {
            return false;
        }
        return Bank.getCount(getId()) > 0;
    }

    public boolean hasIngredientsInBank() {
        if (!Bank.isOpen()) {
            return false;
        }
        for (int index = 0; index < ingredients.length; index++) {
            if (Bank.getCount(ingredients[index].getId()) <= 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isExtreme() {
        return extreme;
    }

    public boolean isOverload() {
        return overload;
    }

}
