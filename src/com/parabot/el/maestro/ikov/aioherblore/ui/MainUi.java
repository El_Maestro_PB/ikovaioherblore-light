package com.parabot.el.maestro.ikov.aioherblore.ui;

import com.parabot.el.maestro.ikov.aioherblore.data.*;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Task;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class MainUi extends JFrame {

    private final JPanel contentPane;
    private final JSpinner decantSpinner = new JSpinner();
    private final ButtonGroup doseChoiceButtonGroup = new ButtonGroup();
    private final JRadioButton fourDose = new JRadioButton("Use (4) doses.");
    private final JComboBox grinderComboBox = new JComboBox();
    private final JSpinner grinderSpinner = new JSpinner();
    private final JSpinner herbCleanerSpinner = new JSpinner();
    private final JComboBox herbToCleanComboBox = new JComboBox();
    private final JSpinner potionMakeSpinner = new JSpinner();
    private final JComboBox potionToDecantComboBox = new JComboBox();
    private final JComboBox potionToMakeComboBox = new JComboBox();
    private final DefaultListModel<String> taskListModel = new DefaultListModel<String>();
    private final JList<String> taskList = new JList<String>(taskListModel);
    private final JRadioButton threeDose = new JRadioButton("Use (3) doses.");

    public MainUi() {
        setTitle("IkovAIOHerblore " + Constants.VERSION_TYPE);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 331, 289);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        final JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
        tabbedPane.setBounds(0, 0, 325, 261);
        contentPane.add(tabbedPane);

        final JPanel panel = new JPanel();
        tabbedPane.addTab("Task List", null, panel, null);
        panel.setLayout(null);

        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 27, 300, 106);
        panel.add(scrollPane);

        taskList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scrollPane.setViewportView(taskList);

        final JLabel lblTaskList = new JLabel("Task List");
        lblTaskList.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblTaskList.setBounds(107, 11, 104, 14);
        panel.add(lblTaskList);

        final JButton btnDeleteTask = new JButton("Delete Task");
        btnDeleteTask.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                taskListModel.removeElementAt(taskList.getSelectedIndex());
            }
        });
        btnDeleteTask.setBounds(10, 144, 150, 23);
        panel.add(btnDeleteTask);

        final JButton btnClearList = new JButton("Clear List");
        btnClearList.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                if (JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the WHOLE list?", "Warning!", JOptionPane.YES_NO_OPTION) == 0) {
                    taskListModel.clear();
                }
            }
        });
        btnClearList.setBounds(160, 144, 150, 23);
        panel.add(btnClearList);

        final JButton btnStartScript = new JButton("Start Script");
        btnStartScript.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                if (!taskListModel.isEmpty()) {
                    for (int index = 0; index < taskListModel.getSize(); index++) {
                        if (taskListModel.get(index).contains("Potion")) {
                            for (final Potion potion : Potion.values()) {
                                if (potion.getName().equalsIgnoreCase(taskListModel.get(index).split("Potion: ")[1].split(",")[0])) {
                                    if (taskListModel.get(index).contains("Make")) {
                                        Variables.addTask(new Task(potion.isOverload() ? Action.MAKE_OVERLOAD : Action.MAKE_POTION, Integer.parseInt(taskListModel.get(index)
                                                .split(": ")[2].split("]")[0]), potion));
                                    } else {
                                        Variables.addTask(new Task(Action.DECANT_POTION, Integer.parseInt(taskListModel.get(index).split(": ")[2].split(", ")[0]), potion, Integer
                                                .parseInt(taskListModel.get(index).split(": ")[3].split("]")[0])));
                                    }
                                }
                            }
                        } else if (taskListModel.get(index).contains("Clean")) {
                            for (final Herb herb : Herb.values()) {
                                if (herb.getName().equalsIgnoreCase(taskListModel.get(index).split(": ")[1].split(",")[0])) {
                                    Variables.addTask(new Task(Action.CLEAN_HERB, Integer.parseInt(taskListModel.get(index).split(": ")[2].split("]")[0]), herb.getCounterPart(),
                                            herb));
                                }
                            }
                        } else {
                            for (final Secondary ingredient : Secondary.values()) {
                                if (ingredient.getName().equalsIgnoreCase(taskListModel.get(index).split(": ")[1].split(",")[0])) {
                                    Variables.addTask(new Task(Action.GRIND_SUPPLIES, Integer.parseInt(taskListModel.get(index).split(": ")[2].split("]")[0]), ingredient));
                                }
                            }
                        }
                    }
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "You have no tasks in your task list. Please make some tasks before starting the script.");
                }
            }
        });
        btnStartScript.setBounds(10, 178, 300, 32);
        panel.add(btnStartScript);

        final JPanel potionMakePanel = new JPanel();
        tabbedPane.addTab("Make Potions", null, potionMakePanel, null);
        potionMakePanel.setLayout(null);

        final JLabel lblPotionToMake = new JLabel("Potion To Make");
        lblPotionToMake.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblPotionToMake.setBounds(89, 14, 128, 14);
        potionMakePanel.add(lblPotionToMake);

        potionToMakeComboBox.setModel(new DefaultComboBoxModel(new String[]{"Attack Potion", "Anti-Poison", "Restore Potion", "Prayer Potion", "Super Attack Potion",
                "Super Strength Potion", "Super Defence Potion", "Magic Potion", "Range Potion", "Saradomin Brew", "Super Restore Potion", "Super Antifire Potion",
                "Recover Special Potion", "Super Prayer Potion", "Extreme Strength Potion", "Extreme Attack Potion", "Extreme Range Potion", "Extreme Defence Potion",
                "Extreme Magic Potion", "Overload"}));
        potionToMakeComboBox.setBounds(84, 39, 143, 20);
        potionMakePanel.add(potionToMakeComboBox);

        final JLabel lblAmountToMake = new JLabel("Amount To Make");
        lblAmountToMake.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblAmountToMake.setBounds(89, 115, 143, 14);
        potionMakePanel.add(lblAmountToMake);

        potionMakeSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        potionMakeSpinner.setBounds(115, 140, 87, 20);
        potionMakePanel.add(potionMakeSpinner);

        final JButton btnNewButton = new JButton("Add Task");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                if (hasPermission(Action.MAKE_POTION, potionToMakeComboBox.getSelectedItem().toString())) {
                    taskListModel.addElement("[Make Potion: " + potionToMakeComboBox.getSelectedItem().toString() + ", Amount: " + (int) potionMakeSpinner.getValue() + "]");
                    potionToMakeComboBox.setSelectedIndex(0);
                    potionMakeSpinner.setValue(1);
                } else {
                    final String[] options = {"Buy Premium(Pro)", "Close"};
                    final int option = JOptionPane.showOptionDialog(null, "Sorry you do not have permission to make " + potionToMakeComboBox.getSelectedItem().toString()
                            + "s because it is a premium feature.", "Error", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                    if (option == 0) {
                        openMarket();
                    }
                }
            }
        });
        btnNewButton.setBounds(31, 187, 248, 23);
        potionMakePanel.add(btnNewButton);

        final JPanel potionDecantPanel = new JPanel();
        tabbedPane.addTab("Make Flasks(Decant)", null, potionDecantPanel, null);
        potionDecantPanel.setLayout(null);

        final JLabel lblPotionToDecant = new JLabel("Potion To Decant");
        lblPotionToDecant.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblPotionToDecant.setBounds(86, 11, 141, 14);
        potionDecantPanel.add(lblPotionToDecant);
        potionToDecantComboBox.setModel(new DefaultComboBoxModel(new String[]{"Attack Potion", "Anti-Poison", "Restore Potion", "Prayer Potion", "Super Attack Potion",
                "Super Strength Potion", "Super Defence Potion", "Magic Potion", "Range Potion", "Super Restore Potion", "Antifire Potion", "Super Energy Potion",
                "Recover Special Potion", "Super Antifire Potion", "Super Prayer Potion", "Saradomin Brew", "Extreme Strength Potion", "Extreme Attack Potion",
                "Extreme Range Potion", "Extreme Defence Potion", "Extreme Magic Potion", "Prayer Renewal Potion","Overload"}));

        potionToDecantComboBox.setBounds(86, 36, 141, 20);
        potionDecantPanel.add(potionToDecantComboBox);

        final JLabel lblAmountOfFlasks = new JLabel("Amount of Flasks to Make");
        lblAmountOfFlasks.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblAmountOfFlasks.setBounds(58, 114, 211, 14);
        potionDecantPanel.add(lblAmountOfFlasks);

        decantSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        decantSpinner.setBounds(87, 142, 141, 20);
        potionDecantPanel.add(decantSpinner);

        final JButton btnAddTask = new JButton("Add Task");
        btnAddTask.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                if (hasPermission(Action.DECANT_POTION, potionToDecantComboBox.getSelectedItem().toString())) {
                    taskListModel.addElement("[Decant Potion: " + potionToDecantComboBox.getSelectedItem().toString() + ", Amount: " + (int) decantSpinner.getValue()
                            + ", Dose to use: " + (threeDose.isSelected() ? 3 : 4) + "]");
                    potionToDecantComboBox.setSelectedIndex(0);
                    decantSpinner.setValue(1);
                } else {
                    final String[] options = {"Buy Premium(Pro)", "Close"};
                    final int option = JOptionPane.showOptionDialog(null, "Sorry you do not have permission to decant " + potionToDecantComboBox.getSelectedItem().toString()
                            + "s because it is a premium feature.", "Error", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                    if (option == 0) {
                        openMarket();
                    }
                }
            }
        });
        btnAddTask.setBounds(22, 173, 275, 23);
        potionDecantPanel.add(btnAddTask);

        threeDose.setSelected(true);
        doseChoiceButtonGroup.add(threeDose);
        threeDose.setBounds(34, 77, 109, 23);
        potionDecantPanel.add(threeDose);

        doseChoiceButtonGroup.add(fourDose);
        fourDose.setBounds(188, 77, 109, 23);
        potionDecantPanel.add(fourDose);

        final JPanel panel_3 = new JPanel();
        tabbedPane.addTab("Clean Herbs", null, panel_3, null);
        panel_3.setLayout(null);

        final JLabel lblHerbToClean = new JLabel("Herb To Clean");
        lblHerbToClean.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblHerbToClean.setBounds(101, 11, 116, 14);
        panel_3.add(lblHerbToClean);
        herbToCleanComboBox.setModel(new DefaultComboBoxModel(
                new String[]{"Grimy Guam", "Grimy Marrentill", "Grimy Tarromin", "Grimy Harralander", "Grimy Ranarr", "Grimy Irit", "Grimy Avantoe", "Grimy Toadflax",
                        "Grimy Snapdragon", "Grimy Kwuarm", "Grimy Lantadyme", "Grimy Dwarf Weed", "Grimy Cadantine", "Grimy Torstol", "Grimy Fellstalk"}));

        herbToCleanComboBox.setBounds(101, 36, 116, 20);
        panel_3.add(herbToCleanComboBox);

        final JLabel lblAmountToClean = new JLabel("Amount To Clean");
        lblAmountToClean.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblAmountToClean.setBounds(89, 86, 141, 20);
        panel_3.add(lblAmountToClean);
        herbCleanerSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));

        herbCleanerSpinner.setBounds(101, 117, 116, 20);
        panel_3.add(herbCleanerSpinner);

        final JButton btnAddTask_1 = new JButton("Add Task");
        btnAddTask_1.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                if (hasPermission(Action.CLEAN_HERB, herbToCleanComboBox.getSelectedItem().toString())) {
                    taskListModel.addElement("[Clean Herb: " + herbToCleanComboBox.getSelectedItem().toString() + ", Amount: " + (int) herbCleanerSpinner.getValue() + "]");
                    herbToCleanComboBox.setSelectedIndex(0);
                    herbCleanerSpinner.setValue(1);
                } else {
                    final String[] options = {"Buy Premium(Pro)", "Close"};
                    final int option = JOptionPane.showOptionDialog(null, "Sorry you do not have permission to clean " + herbToCleanComboBox.getSelectedItem().toString()
                            + "s because it is a premium feature.", "Error", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                    if (option == 0) {
                        openMarket();
                    }
                }
            }
        });
        btnAddTask_1.setBounds(28, 172, 269, 23);
        panel_3.add(btnAddTask_1);

        final JPanel panel_1 = new JPanel();
        tabbedPane.addTab("Grind Supplies", null, panel_1, null);
        panel_1.setLayout(null);

        final JLabel lblNewLabel = new JLabel("Supply To Grind");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblNewLabel.setBounds(97, 11, 129, 29);
        panel_1.add(lblNewLabel);

        grinderComboBox.setModel(new DefaultComboBoxModel(new String[]{"Mud Rune", "Unicorn Horn", "Blue Dragon Scale"}));
        grinderComboBox.setBounds(97, 38, 129, 20);
        panel_1.add(grinderComboBox);

        final JLabel lblAmountToGrind = new JLabel("Amount To Grind");
        lblAmountToGrind.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblAmountToGrind.setBounds(97, 86, 148, 20);
        panel_1.add(lblAmountToGrind);

        grinderSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        grinderSpinner.setBounds(97, 114, 129, 20);
        panel_1.add(grinderSpinner);

        final JButton btnAddTask_2 = new JButton("Add Task");
        btnAddTask_2.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                if (hasPermission(Action.GRIND_SUPPLIES, grinderComboBox.getSelectedItem().toString())) {
                    taskListModel.addElement("[Grind Supply: " + grinderComboBox.getSelectedItem().toString() + ", Amount: " + (int) grinderSpinner.getValue() + "]");
                    grinderComboBox.setSelectedIndex(0);
                    grinderSpinner.setValue(1);
                } else {
                    final String[] options = {"Buy Premium(Pro)", "Close"};
                    final int option = JOptionPane.showOptionDialog(null, "Sorry you do not have permission to grind " + grinderComboBox.getSelectedItem().toString()
                            + "s because it is a premium feature.", "Error", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                    if (option == 0) {
                        openMarket();
                    }
                }
            }
        });
        btnAddTask_2.setBounds(39, 155, 251, 23);
        panel_1.add(btnAddTask_2);
    }

    private boolean hasPermission(final Action action, final String option) {
        switch (action.getActionId()) {
            case 0:
                return !option.equalsIgnoreCase("Grimy Torstol") || option.equalsIgnoreCase("Grimy Torstol") && Constants.VERSION_TYPE.equalsIgnoreCase("pro");
            case 1:
                for (final String str : Constants.PREMIUM_MAKE_GUI_OPTIONS) {
                    if (option.equalsIgnoreCase(str)) {
                        return Constants.VERSION_TYPE.equalsIgnoreCase("pro");
                    }
                }
                return true;
            case 2:
                for (final String str : Constants.PREMIUM_DECANT_GUI_OPTIONS) {
                    if (option.equalsIgnoreCase(str)) {
                        return Constants.VERSION_TYPE.equalsIgnoreCase("pro");
                    }
                }
                return true;
            case 5:
                return option.equalsIgnoreCase("Mud Rune") && Constants.VERSION_TYPE.equalsIgnoreCase("pro") || !option.equalsIgnoreCase("Mud Rune");
            case 4:
            default:
                return false;
        }
    }

    private void openMarket() {
        try {
            Desktop.getDesktop().browse(new URI("http://bdn.parabot.org/scripts/script.php?s=1000047-IkovAIOHerblore-Pro"));
        } catch (IOException | URISyntaxException e1) {
            e1.printStackTrace();
        }
    }
}
