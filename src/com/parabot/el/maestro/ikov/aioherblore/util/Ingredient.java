package com.parabot.el.maestro.ikov.aioherblore.util;

public interface Ingredient {

    public int getBankCount();

    public int getBankStackSize();

    public int getId();

    public int getInventoryCount();

    public int getInventoryStackSize();

    public String getName();

    public boolean hasAmountInBag(int amount);

    public boolean hasAmountInBank(int amount);

    public boolean hasInBag();

    public boolean hasInBank();
}
