package com.parabot.el.maestro.ikov.aioherblore.util;

public enum Action {

    CLEAN_HERB("Clean", 0), DECANT_POTION("Decant", 2), GRIND_SUPPLIES("Grind", 5), MAKE_OVERLOAD("Make", 4), MAKE_POTION("Make", 1), TASK_TRANSITION("Task Transition", 3);

    private int id;
    private String name;

    Action(final String name, final int id) {
        this.name = name;
        this.id = id;
    }

    public int getActionId() {
        return id;
    }

    public String toString() {
        return name;
    }

}
