package com.parabot.el.maestro.ikov.aioherblore.util;

import com.parabot.el.maestro.ikov.aioherblore.data.Herb;
import com.parabot.el.maestro.ikov.aioherblore.data.Potion;
import com.parabot.el.maestro.ikov.aioherblore.data.Secondary;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import org.rev317.min.api.methods.Inventory;

public class Task {

    private final Action action;
    private int actionAmountToPerform;
    private int actionsPerformed;
    private Herb cleanHerb;
    private int doseToDecant;
    private Herb grimyHerb;
    private Secondary ingredientToGrind;
    private Potion potionToDecant;
    private Potion potionToMake;

    public Task(final Action action) {
        this.action = action;
    }

    public Task(final Action action, final int actionAmountToPerform, final Herb cleanHerb, final Herb grimyHerb) {
        this.action = action;
        this.actionAmountToPerform = actionAmountToPerform;
        this.cleanHerb = cleanHerb;
        this.grimyHerb = grimyHerb;
    }

    public Task(final Action action, final int actionAmountToPerform, final Potion potion) {
        this.action = action;
        if (potion.isExtreme() || potion.isOverload()) {
            this.actionAmountToPerform = actionAmountToPerform;
        } else {
            this.actionAmountToPerform = actionAmountToPerform * 2;
        }
        potionToMake = potion;
    }

    public Task(final Action action, final int actionAmountToPerform, final Potion potionToDecant, final int doseToDecant) {
        this.action = action;
        this.potionToDecant = potionToDecant;
        this.doseToDecant = doseToDecant;
        if (doseToDecant == 4) {
            this.actionAmountToPerform = actionAmountToPerform % 2 == 0 ? actionAmountToPerform : actionAmountToPerform - 1;
        } else {
            this.actionAmountToPerform = actionAmountToPerform;
        }
    }

    public Task(final Action action, final int actionAmountToPerform, final Secondary ingredientToGrind) {
        this.action = action;
        this.ingredientToGrind = ingredientToGrind;
        this.actionAmountToPerform = actionAmountToPerform;
    }

    public void addToActionsPerformed(final int amount) {
        switch (action.getActionId()) {
            case 0:
                Variables.setHerbsCleaned(Variables.getHerbsCleaned() + amount);
                break;
            case 1:
                if (!makingUnfinishedPotions()) {
                    Variables.setPotionsMade(Variables.getPotionsMade() + amount);
                } else {
                    Variables.setUnfMade(Variables.getUnfMade() + amount);
                }
                break;
            case 2:
                Variables.setFlasksMade(Variables.getFlasksMade() + amount);
                break;
            case 5:
                Variables.setIngredientsGrinded(Variables.getIngredientsGrinded() + amount);
                break;
        }
        actionsPerformed += amount;
    }

    public Action getAction() {
        return action;
    }

    public int getActionAmountToPerform() {
        return actionAmountToPerform;
    }

    public int getActionsLeftToPerform() {
        return actionAmountToPerform - actionsPerformed;
    }

    public int getActionsPerformed() {
        return actionsPerformed;
    }

    public Herb getCleanedHerb() {
        return cleanHerb;
    }

    public int getDoseToDecant() {
        return doseToDecant;
    }

    public Herb getHerbToClean() {
        return grimyHerb;
    }

    public Secondary getIngredientToGrind() {
        return ingredientToGrind;
    }

    public Potion getPotionToDecant() {
        return potionToDecant;
    }

    public Potion getPotionToMake() {
        return potionToMake;
    }

    public boolean isDone() {
        return actionsPerformed >= actionAmountToPerform && action != Action.TASK_TRANSITION || action == Action.TASK_TRANSITION && Inventory.isEmpty();
    }

    public boolean makingUnfinishedPotions() {
        return !potionToMake.isExtreme() && actionAmountToPerform / 2 > actionsPerformed;
    }

    public void terminate() {
        actionsPerformed = actionAmountToPerform;
    }

    public String toString() {
        switch (action.getActionId()) {
            case 0:
                return action.toString() + " " + actionAmountToPerform + " " + grimyHerb.getName();
            case 1:
                return action.toString() + " " + (potionToMake.isExtreme() ? actionAmountToPerform : actionAmountToPerform / 2) + " " + potionToMake.getName();
            case 2:
                return action.toString() + " " + actionAmountToPerform + " " + potionToDecant.getName();
            case 3:
                return action.toString();
            case 4:
                return action.toString() + " " + actionAmountToPerform + " " + potionToMake.getName();
            case 5:
                return action.toString() + " " + actionAmountToPerform + " " + ingredientToGrind.getName();
        }
        return "Error.";
    }

}
