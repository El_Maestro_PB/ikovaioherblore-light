package com.parabot.el.maestro.ikov.aioherblore.util;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.Loader;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

import java.lang.reflect.Field;

public class Methods {
    public static int getWithdrawX() {
        try {
            final Field lastWithdrawn = Loader.getClient().getClass().getDeclaredField("q");
            lastWithdrawn.setAccessible(true);
            return (int) lastWithdrawn.get(Loader.getClient());
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean setWithdrawX(final int value) {
        try {
            final Field lastWithdrawn = Loader.getClient().getClass().getDeclaredField("q");
            lastWithdrawn.setAccessible(true);
            lastWithdrawn.setInt(Loader.getClient(), value);
            return getWithdrawX() == value;
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void withdrawItem(final int amount, final Item item) {
        if (!Bank.isOpen()) {
            return;
        }
        switch (amount) {
            case -5:
                Menu.sendAction(431, item.getId() - 1, item.getSlot(), 5382, 2213, 4);
                break;
            case 1:
                Menu.sendAction(632, item.getId() - 1, item.getSlot(), 5382, 2213, 4);
                break;
            case 5:
                Menu.sendAction(78, item.getId() - 1, item.getSlot(), 5382, 2213, 4);
                break;
            case 10:
                Menu.sendAction(867, item.getId() - 1, item.getSlot(), 5382, 2213, 4);
                break;
            default:
                if (setWithdrawX(amount)) {
                    Menu.sendAction(776, item.getId() - 1, item.getSlot(), 5382, 2213, 4);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Inventory.getCount(item.getId()) >= amount;
                        }
                    }, 2000);
                }
                break;
        }
    }

}
