package com.parabot.el.maestro.ikov.aioherblore.strategies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Menu;

import java.awt.*;
import java.awt.event.KeyEvent;

public class HandleLogin implements Strategy {

    @Override
    public boolean activate() {
        return !Game.isLoggedIn() || Game.getOpenBackDialogId() == 4900;
    }

    private Point point = new Point(516,134);

    @Override
    public void execute() {
        Mouse.getInstance().click(point);
        if (!Game.isLoggedIn()) {
            Time.sleep(5000);
            Keyboard.getInstance().clickKey(KeyEvent.VK_ENTER);
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Game.isLoggedIn();
                }
            }, 5000);
        }
        if (Game.getOpenBackDialogId() == 4900) {
            Time.sleep(1000);
            Menu.sendAction(679, 17825792, 113, 4907, 1088, 1);
            Time.sleep(500);
        }
    }
}
