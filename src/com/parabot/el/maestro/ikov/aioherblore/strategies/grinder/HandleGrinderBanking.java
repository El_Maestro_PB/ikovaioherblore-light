package com.parabot.el.maestro.ikov.aioherblore.strategies.grinder;

import com.parabot.el.maestro.ikov.aioherblore.data.Constants;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Methods;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public class HandleGrinderBanking implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.GRIND_SUPPLIES) {
            return Bank.isOpen() || Inventory.isEmpty() || !Variables.getCurrentTask().getIngredientToGrind().canGrind();
        }
        return false;
    }

    @Override
    public void execute() {
        Variables.setStatus("Banking...", false);
        if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
            Keyboard.getInstance().sendKeys("::home");
            Time.sleep(4000);
        }
        if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
            while (!Bank.isOpen()) {
                Bank.open(Bank.getBank());
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Bank.isOpen();
                    }
                }, 3000);
            }
        }
        if (Bank.isOpen()) {
            if (Variables.getCurrentTask().getIngredientToGrind().canGrind()) {
                Variables.setStatus("Closing bank...", false);
                Bank.close();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return !Bank.isOpen();
                    }
                }, 1000);
                return;
            } else if (Inventory.isEmpty()) {
                if (Bank.getCount(Constants.PESTLE_AND_MORTAR) >= 1) {
                    final Item pestle = Bank.getItem(Constants.PESTLE_AND_MORTAR);
                    Methods.withdrawItem(1, pestle);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return !Inventory.isEmpty();
                        }
                    }, 1000);
                } else {
                    Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to not having a pestle and mortar", true);
                    Variables.getCurrentTask().terminate();
                    return;
                }
            } else if (Inventory.getCount(Constants.PESTLE_AND_MORTAR) == Inventory.getCount() && !Inventory.isEmpty()) {
                if (Variables.getCurrentTask().getIngredientToGrind().hasInBank()) {
                    if (!Variables.getCurrentTask().getIngredientToGrind().hasInBag()) {
                        final Item ingredient = Bank.getItem(Variables.getCurrentTask().getIngredientToGrind().getId());
                        Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() > 27 ? 27 : Variables.getCurrentTask().getActionsLeftToPerform(), ingredient);
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return Variables.getCurrentTask().getIngredientToGrind().hasInBag();
                            }
                        }, 1000);
                    }
                } else {
                    Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to insuficient supplies.", true);
                    Variables.getCurrentTask().terminate();
                    return;
                }
            } else if (!Inventory.isEmpty() && !Variables.getCurrentTask().getIngredientToGrind().canGrind()) {
                Variables.setStatus("Clearing inventory...", false);
                Bank.depositAllExcept(Constants.PESTLE_AND_MORTAR);
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Inventory.isEmpty();
                    }
                }, 2000);
            }
        }
    }

}
