package com.parabot.el.maestro.ikov.aioherblore.strategies.making.special;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Ingredient;
import com.parabot.el.maestro.ikov.aioherblore.util.Methods;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public class HandleOverloadMakerBanking implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.MAKE_OVERLOAD) {
            return !Inventory.isEmpty() && Variables.getCurrentTask().getPotionToMake().getInventoryCount() == Inventory.getCount() || Bank.isOpen() || Inventory.isEmpty();
        }
        return false;
    }

    @Override
    public void execute() {
        Variables.setStatus("Banking...", false);
        if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
            Keyboard.getInstance().sendKeys("::home");
            Time.sleep(4000);
        }
        if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
            while (!Bank.isOpen()) {
                Bank.open(Bank.getBank());
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Bank.isOpen();
                    }
                }, 3000);
            }
        }
        if (Bank.isOpen()) {
            if (!Inventory.isEmpty() && Variables.getCurrentTask().getPotionToMake().hasInBag()) {
                Variables.setStatus("Clearing inventory...", false);
                Bank.depositAllExcept();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Inventory.isEmpty();
                    }
                }, 2000);
            } else if (Variables.getCurrentTask().getPotionToMake().canMake()) {
                Variables.setStatus("Closing bank...", false);
                Bank.close();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return !Bank.isOpen();
                    }
                }, 1000);
                return;
            } else if (Variables.getCurrentTask().getPotionToMake().hasIngredientsInBank()) {
                if (!Variables.getCurrentTask().getPotionToMake().canMake()) {
                    for (final Ingredient ingredient : Variables.getCurrentTask().getPotionToMake().getIngredients()) {
                        if (!ingredient.hasInBag()) {
                            final Item bankIngredient = Bank.getItem(ingredient.getId());
                            Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() > 4 ? 4 : Variables.getCurrentTask().getActionsLeftToPerform(),
                                    bankIngredient);
                            Time.sleep(new SleepCondition() {
                                @Override
                                public boolean isValid() {
                                    return ingredient.hasInBag();
                                }
                            }, 1000);
                        }
                    }
                }
            } else {
                Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to insuficient supplies.", true);
                Variables.getCurrentTask().terminate();
                return;
            }
        }
    }

}
