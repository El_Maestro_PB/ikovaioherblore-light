package com.parabot.el.maestro.ikov.aioherblore.strategies.grinder;

import com.parabot.el.maestro.ikov.aioherblore.data.Constants;
import com.parabot.el.maestro.ikov.aioherblore.data.Secondary;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.*;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.Item;

public class GrindSupplies implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.GRIND_SUPPLIES) {
            return Players.getMyPlayer().getAnimation() == -1 && !Bank.isOpen() && Variables.getCurrentTask().getIngredientToGrind().canGrind();
        }
        return false;
    }

    @Override
    public void execute() {
        if (Variables.getCurrentTask().getIngredientToGrind().canGrind()) {
            Logger.addMessage("Grinding ingredient...", false);
            if (Game.getOpenBackDialogId() == -1) {
                final Item pestle = Inventory.getItem(Constants.PESTLE_AND_MORTAR);
                final Item ingredient = Inventory.getItem(Variables.getCurrentTask().getIngredientToGrind().getId());
                if (pestle != null && ingredient != null) {
                    pestle.interact(Option.USE);
                    Time.sleep(300);
                    ingredient.interact(Option.USE_WITH);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Game.getOpenBackDialogId() != -1;
                        }
                    }, 2000);
                }

            } else if (Game.getOpenBackDialogId() == Constants.MAKE_DIALOG) {
                Menu.clickButton(1747);
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Game.getOpenBackDialogId() == -1 && !Variables.getCurrentTask().getIngredientToGrind().canGrind();
                    }
                }, 1300 * (Variables.getCurrentTask().getIngredientToGrind() == Secondary.MUD_RUNE ? Variables.getCurrentTask().getIngredientToGrind().getInventoryStackSize()
                        : Variables.getCurrentTask().getIngredientToGrind().getInventoryCount()));
            }
        }
    }

}
