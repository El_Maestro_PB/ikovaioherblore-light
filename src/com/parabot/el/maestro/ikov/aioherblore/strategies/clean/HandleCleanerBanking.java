package com.parabot.el.maestro.ikov.aioherblore.strategies.clean;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Methods;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public class HandleCleanerBanking implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.CLEAN_HERB) {
            return Inventory.isEmpty() || !Inventory.isEmpty() && !Variables.getCurrentTask().getHerbToClean().hasInBag() || Bank.isOpen();
        }
        return false;
    }

    @Override
    public void execute() {
        Variables.setStatus("Banking...", false);
        if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
            Keyboard.getInstance().sendKeys("::home");
            Time.sleep(4000);
        }
        if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
            while (!Bank.isOpen()) {
                Bank.open(Bank.getBank());
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Bank.isOpen();
                    }
                }, 3000);
            }
        }
        if (Bank.isOpen()) {
            if (Variables.getCurrentTask().getHerbToClean().hasInBag()) {
                Variables.setStatus("Closing bank...", false);
                Bank.close();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return !Bank.isOpen();
                    }
                }, 1000);
                return;
            }
            if (!Inventory.isEmpty() && Variables.getCurrentTask().getCleanedHerb().hasInBag()) {
                Variables.setStatus("Clearing inventory...", false);
                Bank.depositAllExcept();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Inventory.isEmpty();
                    }
                }, 2000);
            }
            if (Variables.getCurrentTask().getHerbToClean().hasInBank()) {
                if (Inventory.isEmpty()) {
                    final Item herb = Bank.getItem(Variables.getCurrentTask().getHerbToClean().getId());
                    Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() > 28 ? 28 : Variables.getCurrentTask().getActionsLeftToPerform(), herb);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return !Inventory.isEmpty();
                        }
                    }, 2000);
                }
            } else {
                Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to insuficient supplies.", true);
                Variables.getCurrentTask().terminate();
                return;
            }
        }
    }

}
