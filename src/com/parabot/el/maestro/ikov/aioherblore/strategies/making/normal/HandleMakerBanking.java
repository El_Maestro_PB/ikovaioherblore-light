package com.parabot.el.maestro.ikov.aioherblore.strategies.making.normal;

import com.parabot.el.maestro.ikov.aioherblore.data.Potion;
import com.parabot.el.maestro.ikov.aioherblore.data.Secondary;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Methods;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public class HandleMakerBanking implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.MAKE_POTION) {
            return Bank.isOpen()
                    || !Inventory.isEmpty()
                    && (Variables.getCurrentTask().getPotionToMake().getInventoryCount() == Inventory.getCount()
                    || !Variables.getCurrentTask().getPotionToMake().getPrimary().hasInBag() && Variables.getCurrentTask().getPotionToMake().isExtreme() || !Variables
                    .getCurrentTask().getPotionToMake().isExtreme()
                    && Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().getInventoryCount() == Inventory.getCount()) || Inventory.isEmpty();
        }
        return false;
    }

    @Override
    public void execute() {
        Variables.setStatus("Banking...", false);
        if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
            Keyboard.getInstance().sendKeys("::home");
            Time.sleep(4000);
        }
        if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
            while (!Bank.isOpen()) {
                Bank.open(Bank.getBank());
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Bank.isOpen();
                    }
                }, 3000);
            }
        }
        if (Bank.isOpen()) {
            if (!Inventory.isEmpty()
                    && (!Variables.getCurrentTask().getPotionToMake().isExtreme()
                    && Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().getInventoryCount() == Inventory.getCount() || Variables.getCurrentTask()
                    .getPotionToMake().getInventoryCount() == Inventory.getCount())) {
                Variables.setStatus("Clearing inventory...", false);
                Bank.depositAllExcept();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Inventory.isEmpty();
                    }
                }, 2000);
            } else if (Variables.getCurrentTask().makingUnfinishedPotions() && !Variables.getCurrentTask().getPotionToMake().isExtreme()) {
                if (Secondary.VIAL.hasInBag() && Variables.getCurrentTask().getPotionToMake().getPrimary().hasInBag()) {
                    Variables.setStatus("Closing bank...", false);
                    Bank.close();
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return !Bank.isOpen();
                        }
                    }, 1000);
                    return;
                }
                if (Secondary.VIAL.hasInBank() && Variables.getCurrentTask().getPotionToMake().getPrimary().hasInBank()) {
                    if (Inventory.isEmpty()) {
                        final Item primary = Bank.getItem(Variables.getCurrentTask().getPotionToMake().getPrimary().getId());
                        Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() / 2 > 14 ? 14 : Variables.getCurrentTask().getActionsLeftToPerform() / 2, primary);
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return !Inventory.isEmpty();
                            }
                        }, 2000);
                    }
                    if (!Inventory.isEmpty()) {
                        final Item vial = Bank.getItem(Secondary.VIAL.getId());
                        Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() / 2 > 14 ? 14 : Variables.getCurrentTask().getActionsLeftToPerform() / 2, vial);
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return Secondary.VIAL.hasInBag();
                            }
                        }, 1000);
                    }
                } else {
                    Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to insuficient supplies.", true);
                    Variables.getCurrentTask().terminate();
                    return;
                }
            } else {
                if (Variables.getCurrentTask().getPotionToMake().canMake()) {
                    Variables.setStatus("Closing bank...", false);
                    Bank.close();
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return !Bank.isOpen();
                        }
                    }, 1000);
                    return;
                }
                if (!Variables.getCurrentTask().getPotionToMake().isExtreme()
                        && Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().hasInBank()
                        && Variables.getCurrentTask().getPotionToMake().getSecondary().hasInBank()
                        || Variables.getCurrentTask().getPotionToMake().isExtreme()
                        && Variables.getCurrentTask().getPotionToMake().getPrimary().hasInBank()
                        && (Variables.getCurrentTask().getPotionToMake() == Potion.EXTREME_RANGE_POTION ? Variables.getCurrentTask().getPotionToMake().getSecondary()
                        .getBankStackSize() >= 5 : Variables.getCurrentTask().getPotionToMake().getSecondary().hasInBank())) {
                    if (Inventory.isEmpty()) {
                        if (!Variables.getCurrentTask().getPotionToMake().isExtreme()) {
                            final Item unfinishedPotion = Bank.getItem(Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().getId());
                            Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() >= 14 ? 14 : Variables.getCurrentTask().getActionsLeftToPerform(),
                                    unfinishedPotion);
                        } else {
                            final Item primary = Bank.getItem(Variables.getCurrentTask().getPotionToMake().getPrimary().getId());
                            if (Variables.getCurrentTask().getPotionToMake() == Potion.EXTREME_RANGE_POTION) {
                                Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() >= 27 ? 27 : Variables.getCurrentTask().getActionsLeftToPerform(),
                                        primary);
                            } else {
                                Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() >= 14 ? 14 : Variables.getCurrentTask().getActionsLeftToPerform(),
                                        primary);
                            }
                        }
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return !Variables.getCurrentTask().getPotionToMake().isExtreme() && Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().hasInBag()
                                        || Variables.getCurrentTask().getPotionToMake().getPrimary().hasInBag();
                            }
                        }, 1000);
                    }
                    if (!Inventory.isEmpty() && !Variables.getCurrentTask().getPotionToMake().getSecondary().hasInBag()) {
                        final Item secondary = Bank.getItem(Variables.getCurrentTask().getPotionToMake().getSecondary().getId());
                        if (Variables.getCurrentTask().getPotionToMake() == Potion.EXTREME_RANGE_POTION) {
                            Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() >= 27 ? 135 : Variables.getCurrentTask().getActionsLeftToPerform() * 5,
                                    secondary);
                        } else {
                            Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() >= 14 ? 14 : Variables.getCurrentTask().getActionsLeftToPerform(), secondary);
                        }
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return Variables.getCurrentTask().getPotionToMake().getSecondary().hasInBag();
                            }
                        }, 1000);
                    }
                } else {
                    Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to insuficient supplies.", true);
                    Variables.getCurrentTask().terminate();
                    return;
                }
            }
        }
    }
}
