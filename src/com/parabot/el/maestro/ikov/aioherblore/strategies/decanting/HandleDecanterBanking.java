package com.parabot.el.maestro.ikov.aioherblore.strategies.decanting;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.Methods;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

public class HandleDecanterBanking implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.DECANT_POTION) {
            return !Variables.getCurrentTask().getPotionToDecant().canDecant() || Inventory.isEmpty() || !Bank.isOpen();
        }
        return false;
    }

    @Override
    public void execute() {
        Variables.setStatus("Banking...", false);
        if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
            Keyboard.getInstance().sendKeys("::home");
            Time.sleep(4000);
        }
        if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
            while (!Bank.isOpen()) {
                Bank.open(Bank.getBank());
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Bank.isOpen();
                    }
                }, 3000);
            }
        }
        if (Bank.isOpen() && !Variables.getCurrentTask().getPotionToDecant().canDecant()) {
            if (!Inventory.isEmpty()) {
                Variables.setStatus("Clearing inventory...", false);
                Bank.depositAllExcept();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Inventory.isEmpty();
                    }
                }, 2000);
            } else if (Variables.getCurrentTask().getPotionToDecant().hasInBank()) {
                if (Inventory.isEmpty()) {
                    final Item potion = Bank.getItem(Variables.getCurrentTask().getPotionToDecant().getId());
                    if (Variables.getCurrentTask().getActionAmountToPerform() == Variables.getCurrentTask().getActionsLeftToPerform() && Variables.getPotionBankCount() == 0) {
                        Variables.setPotionBankCount(potion.getStackSize());
                    }
                    if (Variables.getCurrentTask().getDoseToDecant() == 3) {
                        Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() > 14 ? -5 : Variables.getCurrentTask().getActionsLeftToPerform() * 2, potion);
                    } else {
                        Methods.withdrawItem(Variables.getCurrentTask().getActionsLeftToPerform() > 18 ? 27 : Variables.getCurrentTask().getActionsLeftToPerform() * 6 / 4, potion);
                    }
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return !Inventory.isEmpty();
                        }
                    }, 2000);
                }
            } else {
                Logger.addMessage("The current task: " + Variables.getCurrentTask().toString() + " has been terminated due to insuficient supplies.", true);
                Variables.getCurrentTask().terminate();
                return;
            }
        }
    }
}
