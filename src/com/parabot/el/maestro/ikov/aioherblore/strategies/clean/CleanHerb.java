package com.parabot.el.maestro.ikov.aioherblore.strategies.clean;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

public class CleanHerb implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.CLEAN_HERB) {
            return Variables.getCurrentTask().getHerbToClean().hasInBag();
        }
        return false;
    }

    @Override
    public void execute() {
        if (Variables.getCurrentTask().getHerbToClean().hasInBag()) {
            Variables.setStatus("Cleaning " + Variables.getCurrentTask().getHerbToClean().getName() + "...", false);
            for (final Item herb : Inventory.getItems(Variables.getCurrentTask().getHerbToClean().getId())) {
                if (herb != null) {
                    Menu.sendAction(74, herb.getId() - 1, herb.getSlot(), 3214, 0);
                    Time.sleep(100);
                }
            }
        }
    }

}
