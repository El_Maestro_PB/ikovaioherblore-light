package com.parabot.el.maestro.ikov.aioherblore.strategies.decanting;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.Item;

public class DecantPotion implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.DECANT_POTION) {
            return Variables.getCurrentTask().getPotionToDecant().canDecant() && Bank.isOpen();
        }
        return false;
    }

    @Override
    public void execute() {
        if (Variables.getPotionBankCount() != 0) {
            final Item potion = Bank.getItem(Variables.getCurrentTask().getPotionToDecant().getId());
            if (potion != null) {
                if (Variables.getCurrentTask().getDoseToDecant() == 3) {
                    Variables.getCurrentTask().addToActionsPerformed((Variables.getPotionBankCount() - potion.getStackSize()) / 2 - Variables.getFlasksMade());
                } else {
                    Variables.getCurrentTask().addToActionsPerformed((Variables.getPotionBankCount() - potion.getStackSize()) * 4 / 6 - Variables.getFlasksMade());
                }
            }
        }
        Variables.setStatus("Making " + Variables.getCurrentTask().getPotionToDecant().getName() + " flasks...", false);
        final Item potionOne = Inventory.getItem(Variables.getCurrentTask().getPotionToDecant().getId());
        final Item potionTwo = Inventory.getItem(Variables.getCurrentTask().getPotionToDecant().getId());
        if (potionOne != null && potionTwo != null) {
            potionOne.interact(Option.USE);
            Time.sleep(650);
            potionTwo.interact(Option.USE_WITH);
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Inventory.getCount(Variables.getCurrentTask().getPotionToDecant().getId()) < Inventory.getCount();
                }
            }, 1000);
        }
    }

}
