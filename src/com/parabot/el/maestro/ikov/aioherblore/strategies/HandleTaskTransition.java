package com.parabot.el.maestro.ikov.aioherblore.strategies;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;

public class HandleTaskTransition implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.TASK_TRANSITION) {
            return !Inventory.isEmpty();
        }
        return false;
    }

    @Override
    public void execute() {
        Variables.setStatus("Banking...", false);
        if (Bank.getNearestBanks().length == 0 || Bank.getNearestBanks()[0].distanceTo() > 7) {
            Keyboard.getInstance().sendKeys("::home");
            Time.sleep(4000);
        }
        if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {
            while (!Bank.isOpen()) {
                Bank.open(Bank.getBank());
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Bank.isOpen();
                    }
                }, 3000);
            }
        }
        if (Bank.isOpen()) {
            Variables.setStatus("Clearing inventory...", false);
            Bank.depositAllExcept();
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Inventory.isEmpty();
                }
            }, 2000);
        }
    }

}
