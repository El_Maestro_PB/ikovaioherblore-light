package com.parabot.el.maestro.ikov.aioherblore.strategies;

import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import org.parabot.core.Context;
import org.parabot.core.ui.Logger;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.framework.Strategy;

public class HandleTasks implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getTasks() == null || Variables.getTasks().isEmpty() || Variables.getCurrentTask() != null && Variables.getCurrentTask().isDone()
                && Variables.getTasks().size() == 1) {
            Logger.addMessage("All tasks have been completed. Thank you for using IkovAIOHerblore!", true);
            Context.getInstance().getRunningScript().setState(Script.STATE_STOPPED);
            return false;
        }
        return Variables.getCurrentTask() == null || Variables.getCurrentTask().isDone();
    }

    @Override
    public void execute() {
        if (Variables.getCurrentTask() != null && Variables.getCurrentTask().isDone()) {
            Variables.getTasks().remove(Variables.getCurrentTask());
            Logger.addMessage("The current task has been finished. Removing it from the task list.", true);
            Logger.addMessage("Moving to the next task...", true);
        }
        Variables.setPotionBankCount(0);
        Variables.setCurrentTask(Variables.getTasks().get(0));
        Logger.addMessage("The current task has now been set to: " + Variables.getCurrentTask().toString(), true);
    }

}
