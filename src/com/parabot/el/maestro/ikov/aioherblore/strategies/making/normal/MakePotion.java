package com.parabot.el.maestro.ikov.aioherblore.strategies.making.normal;

import com.parabot.el.maestro.ikov.aioherblore.data.Constants;
import com.parabot.el.maestro.ikov.aioherblore.data.Secondary;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.*;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.Item;

public class MakePotion implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.MAKE_POTION) {
            return Players.getMyPlayer().getAnimation() == -1
                    && !Bank.isOpen()
                    && (!Variables.getCurrentTask().getPotionToMake().isExtreme() && Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().canMake() || Variables
                    .getCurrentTask().getPotionToMake().canMake());
        }
        return false;
    }

    @Override
    public void execute() {
        if (Variables.getCurrentTask().makingUnfinishedPotions()) {
            if (Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().canMake()) {
                Variables.setStatus("Making unfinished potions...", false);
                if (Game.getOpenBackDialogId() == -1) {
                    final Item vial = Inventory.getItem(Secondary.VIAL.getId());
                    final Item primary = Inventory.getItem(Variables.getCurrentTask().getPotionToMake().getPrimary().getId());
                    if (vial != null && primary != null) {
                        vial.interact(Option.USE);
                        Time.sleep(300);
                        primary.interact(Option.USE_WITH);
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return Game.getOpenBackDialogId() != -1;
                            }
                        }, 2000);
                    }
                } else if (Game.getOpenBackDialogId() == Constants.MAKE_DIALOG) {
                    Menu.clickButton(1747);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Game.getOpenBackDialogId() == -1 && !Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().canMake()
                                    && Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().hasInBag();
                        }
                    }, 1300 * Variables.getCurrentTask().getPotionToMake().getPrimary().getInventoryCount());
                }
            }
        } else {
            if (Variables.getCurrentTask().getPotionToMake().canMake()) {
                Variables.setStatus("Making potions...", false);
                if (Game.getOpenBackDialogId() == -1) {
                    final Item secondary = Inventory.getItem(Variables.getCurrentTask().getPotionToMake().getSecondary().getId());
                    secondary.interact(Option.USE);
                    Time.sleep(300);
                    if (Variables.getCurrentTask().getPotionToMake().isExtreme()) {
                        final Item potion = Inventory.getItem(Variables.getCurrentTask().getPotionToMake().getPrimary().getId());
                        potion.interact(Option.USE_WITH);
                    } else {
                        final Item unfinishedPotion = Inventory.getItem(Variables.getCurrentTask().getPotionToMake().getUnfinishedPotion().getId());
                        if (unfinishedPotion != null && secondary != null) {
                            unfinishedPotion.interact(Option.USE_WITH);
                        }
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return Game.getOpenBackDialogId() != -1;
                            }
                        }, 2000);
                    }

                } else if (Game.getOpenBackDialogId() == Constants.MAKE_DIALOG) {
                    Menu.clickButton(1747);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Game.getOpenBackDialogId() == -1 && !Variables.getCurrentTask().getPotionToMake().canMake()
                                    && Variables.getCurrentTask().getPotionToMake().hasInBag() && Game.getOpenBackDialogId() != Constants.MAKE_DIALOG;
                        }
                    }, 1400 * (Variables.getCurrentTask().getPotionToMake().isExtreme() ? Variables.getCurrentTask().getPotionToMake().getPrimary().getInventoryCount() : Variables
                            .getCurrentTask().getPotionToMake().getSecondary().getInventoryCount()));
                }
            }
        }
    }

}
