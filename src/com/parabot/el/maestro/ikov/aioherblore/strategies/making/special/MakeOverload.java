package com.parabot.el.maestro.ikov.aioherblore.strategies.making.special;

import com.parabot.el.maestro.ikov.aioherblore.data.Constants;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.*;
import org.rev317.min.api.methods.Items.Option;
import org.rev317.min.api.wrappers.Item;

public class MakeOverload implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getCurrentTask().getAction() == Action.MAKE_OVERLOAD) {
            return Players.getMyPlayer().getAnimation() == -1 && !Bank.isOpen() && Variables.getCurrentTask().getPotionToMake().canMake();
        }
        return false;
    }

    @Override
    public void execute() {
        if (Variables.getCurrentTask().getPotionToMake().canMake()) {
            Logger.addMessage("Making potion...", false);
            if (Game.getOpenBackDialogId() == -1) {
                final Item herb = Inventory.getItem(Variables.getCurrentTask().getPotionToMake().getIngredients()[0].getId());
                final Item potion = Inventory.getItem(Variables.getCurrentTask().getPotionToMake().getIngredients()[0].getId());
                if (herb != null && potion != null) {
                    herb.interact(Option.USE);
                    Time.sleep(300);
                    potion.interact(Option.USE_WITH);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Game.getOpenBackDialogId() != -1;
                        }
                    }, 2000);
                }

            } else if (Game.getOpenBackDialogId() == Constants.MAKE_DIALOG) {
                Menu.clickButton(1747);
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Game.getOpenBackDialogId() == -1 && !Variables.getCurrentTask().getPotionToMake().canMake()
                                && Variables.getCurrentTask().getPotionToMake().hasInBag();
                    }
                }, 1300 * Variables.getCurrentTask().getPotionToMake().getIngredients()[0].getInventoryCount());
            }
        }
    }

}
