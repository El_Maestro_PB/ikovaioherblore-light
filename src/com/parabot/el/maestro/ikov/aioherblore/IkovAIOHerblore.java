package com.parabot.el.maestro.ikov.aioherblore;

import com.parabot.el.maestro.ikov.aioherblore.data.Constants;
import com.parabot.el.maestro.ikov.aioherblore.data.Variables;
import com.parabot.el.maestro.ikov.aioherblore.strategies.HandleLogin;
import com.parabot.el.maestro.ikov.aioherblore.strategies.HandleTaskTransition;
import com.parabot.el.maestro.ikov.aioherblore.strategies.HandleTasks;
import com.parabot.el.maestro.ikov.aioherblore.strategies.clean.CleanHerb;
import com.parabot.el.maestro.ikov.aioherblore.strategies.clean.HandleCleanerBanking;
import com.parabot.el.maestro.ikov.aioherblore.strategies.decanting.DecantPotion;
import com.parabot.el.maestro.ikov.aioherblore.strategies.decanting.HandleDecanterBanking;
import com.parabot.el.maestro.ikov.aioherblore.strategies.grinder.GrindSupplies;
import com.parabot.el.maestro.ikov.aioherblore.strategies.grinder.HandleGrinderBanking;
import com.parabot.el.maestro.ikov.aioherblore.strategies.making.normal.HandleMakerBanking;
import com.parabot.el.maestro.ikov.aioherblore.strategies.making.normal.MakePotion;
import com.parabot.el.maestro.ikov.aioherblore.strategies.making.special.HandleOverloadMakerBanking;
import com.parabot.el.maestro.ikov.aioherblore.strategies.making.special.MakeOverload;
import com.parabot.el.maestro.ikov.aioherblore.ui.MainUi;
import com.parabot.el.maestro.ikov.aioherblore.util.Action;
import com.parabot.el.maestro.ikov.aioherblore.util.ScriptTimer;
import org.parabot.core.Context;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.randoms.Random;
import org.rev317.min.Loader;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;
import org.rev317.min.api.methods.Skill;

import java.awt.*;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

@ScriptManifest(author = "El Maestro", category = Category.HERBLORE, description = "An all in one herblore script for ikov.", name = "IkovAIOHerblore " + Constants.VERSION_TYPE, servers = {"Ikov"}, version = Constants.VERSION)
public class IkovAIOHerblore extends Script implements MessageListener, Paintable {

    private final Color color1 = new Color(0, 102, 0, 118);
    private final Color color2 = new Color(0, 0, 0);
    private final Color color4 = new Color(255, 0, 0);
    private final Font font1 = new Font("Arial", 1, 16);
    private final Font font2 = new Font("Arial", 1, 12);
    private final Font font4 = new Font("Arial", 1, 13);
    private final BasicStroke stroke1 = new BasicStroke(1);
    private MainUi gui;
    private boolean runPaint = false;
    private int taskPaintSpacing = 196;
    private ScriptTimer timer;

    public static String formatNumber(final int start) {
        final DecimalFormat nf = new DecimalFormat("0.0");
        final double i = start;
        if (i >= 1000000) {
            return nf.format(i / 1000000) + "M";
        }
        if (i >= 1000) {
            return nf.format(i / 1000) + "K";
        }
        return "" + start;
    }

    private void dropClient() {
        try {
            final Method dropClient = Loader.getClient().getClass().getDeclaredMethod("W");
            dropClient.setAccessible(true);
            dropClient.invoke(Loader.getClient());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageReceived(final MessageEvent message) {
        if (message.getType() == 0) {
            if (Variables.getCurrentTask() != null) {
                if (message.getMessage().contains("make a") || message.getMessage().contains("grind") || message.getMessage().contains("clean the")
                        | message.getMessage().contains("mix the")) {
                    Variables.getCurrentTask().addToActionsPerformed(1);
                }
            } else if (message.getMessage().contains("object does not exist") || message.getMessage().contains("is already on your") || message.getMessage().contains("you cannot do this") || message.getMessage().contains("Walk fix[Debug]") || message.getMessage().contains("command") || message.getMessage().contains("clan chat")) {
                Logger.addMessage("Your account has been nulled. Relogging to fix the issue...", true);
                dropClient();
                Time.sleep(1000);
            }
        }
    }

    public boolean onExecute() {
        ArrayList<Random> randoms = Context.getInstance().getRandomHandler().getActiveRandoms();
        ArrayList<Random> r = new ArrayList<Random>();
        for (Random random : randoms) {
            if (random.getName().equalsIgnoreCase("login")) {
                r.add(random);
            }
        }
        randoms.removeAll(r);
        gui = new MainUi();
        gui.setVisible(true);
        while (gui.isVisible()) {
            Time.sleep(500);
        }
        timer = new ScriptTimer(Skill.HERBLORE);
        runPaint = true;
        Logger.addMessage("IkovAIOHerblore " + Constants.VERSION_TYPE + " has started. Enjoy the script!", true);
        provide(Arrays.asList(new HandleLogin(), new HandleTasks(), new HandleTaskTransition(), new HandleMakerBanking(), new MakePotion(), new HandleCleanerBanking(),
                new CleanHerb(), new HandleDecanterBanking(), new DecantPotion(), new HandleOverloadMakerBanking(), new MakeOverload(), new HandleGrinderBanking(),
                new GrindSupplies()));
        return true;
    }

    public void onFinish() {
        Logger.addMessage("IkovAIOHerblore has stopped running. Thank you for using the script!", true);
    }

    @Override
    public void paint(final Graphics g1) {
        if (runPaint) {
            final Graphics2D g = (Graphics2D) g1;
            if (Variables.getTasks() != null && !Variables.getTasks().isEmpty()) {
                g.setFont(font4);
                g.setColor(color4);
                taskPaintSpacing = 196;
                for (int i = 0; i < Variables.getTasks().size(); i++) {
                    if (Variables.getTasks().get(i).getAction() != Action.TASK_TRANSITION) {
                        g.drawString("" + Variables.getTasks().get(i).toString(), 277, 196 + i * 9);
                        taskPaintSpacing += 18;
                    }
                }
            }
            g.setColor(color1);
            g.fillRect(273, 3, 244, 190 + (Variables.getTasks() != null ? Variables.getTasks().size() * 9 : 0));
            g.setColor(color2);
            g.setStroke(stroke1);
            g.drawRect(273, 3, 244, 190 + (Variables.getTasks() != null ? Variables.getTasks().size() * 9 : 0));
            g.setFont(font1);
            g.drawString("IkovAIOHerblore " + Constants.VERSION_TYPE, 311, 18);
            g.setFont(font2);
            g.drawString("Potions Made(P/H): " + formatNumber(Variables.getPotionsMade()) + "(" + formatNumber(timer.getPerHour(Variables.getPotionsMade())) + ")", 277, 55);
            g.drawString("Herbs Cleaned(P/H): " + formatNumber(Variables.getHerbsCleaned()) + "(" + formatNumber(timer.getPerHour(Variables.getHerbsCleaned())) + ")", 277, 103);
            g.drawString("Flasks Made(P/H): " + formatNumber(Variables.getFlasksMade()) + "(" + formatNumber(timer.getPerHour(Variables.getFlasksMade())) + ")", 277, 87);
            g.drawLine(368, 181, 418, 181);
            g.drawString("Time: " + timer.toString(), 277, 39);
            g.drawString("Version: " + Constants.VERSION, 551, 462);
            g.drawString("Task List", 367, 179);
            g.drawString("Actions Performed(P/H): " + formatNumber(Variables.getActionsPerformed()) + "(" + formatNumber(timer.getPerHour(Variables.getActionsPerformed())) + ")",
                    277, 135);
            g.drawString("Supplies Grinded(P/H): " + formatNumber(Variables.getIngredientsGrinded()) + "(" + formatNumber(timer.getPerHour(Variables.getIngredientsGrinded()))
                    + ")", 277, 119);
            g.drawString("Unf Potions Made(P/H): " + formatNumber(Variables.getUnfMade()) + "(" + formatNumber(timer.getPerHour(Variables.getUnfMade())) + ")", 277, 71);
            g.drawLine(274, 174, 367, 174);
            g.drawLine(517, 174, 419, 174);
            g.drawString("Herblore Exp(P/H): " + formatNumber(timer.getXpGained()) + "(" + formatNumber(timer.getPerHour(timer.getXpGained())) + ")", 277, 151);
            g.drawString("Status: " + Variables.getStatus(), 277, 167);
        }
    }
}
